import re


class Phrase:
    def __init__(self, phrase_dict):
        self.name = phrase_dict["name"]
        self.regex = re.compile(phrase_dict["regex"])
        self.count = 0
