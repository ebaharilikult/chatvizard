from wordcloud import WordCloud
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import seaborn as sns
import pandas as pd
from IPython.display import display_html


def show_word_cloud(sender):
    bubble_mask = np.array(Image.open("resources/fumetto-pixilart.png"))
    wordcloud = WordCloud(background_color="white", mask=bubble_mask)
    wordcloud.generate_from_frequencies(frequencies=dict(sender.vocab))
    plt.figure()
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    print(f"{sender.name}s WordCloud looks like this: ")
    plt.show()


def msg_comparison(A, B):
    labels = ['Text Messages', "Words (1:10)", 'Media Posts', ]
    A_values = [A.message_count,  (A.word_count / 10), A.media_count]
    B_values = [B.message_count,  (B.word_count / 10), B.media_count]

    x = np.arange(len(labels))  # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width/2, A_values, width, label=A.name)
    rects2 = ax.bar(x + width/2, B_values, width, label=B.name)

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Total count')
    ax.set_title('Comparison of various post types')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()

    autolabel(rects1, ax)
    autolabel(rects2, ax)

    fig.tight_layout()

    plt.show()
    print(
        f"{A.name} wrote an average of {str(A.avg_message_size)[:4]} words per message.\n{B.name} wrote an average of {str(B.avg_message_size)[:4]} words per message.\n")
    print(f"The longest message by {A.name} was {A.max_message_size} words long, written at {A.max_message_size_time_timestamp}.\nThe longest message by {B.name} was {B.max_message_size} words long, written at {B.max_message_size_time_timestamp}.\n ")


def weekday_comparison(A, B):
    labels = ["Monday", "Tuesday", 'Wednesday',
              "Thursday", "Friday", "Saturday", "Sunday"]
    A_values = [round(x, 2) for x in A.weekday_words.values()]
    B_values = [round(x, 2) for x in B.weekday_words.values()]

    x = np.arange(len(labels))  # the label locations
    width = 0.25  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width/2, A_values, width, label=A.name)
    rects2 = ax.bar(x + width/2, B_values, width, label=B.name)

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Average Words')
    ax.set_title('Average words sent across the week')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()

    autolabel(rects1, ax)
    autolabel(rects2, ax)

    fig.tight_layout()
    plt.show()


def weekday_comparison_resp(A, B):
    if A.avg_response_time < B.avg_response_time:
        print(f"Across all messages, {A.name} has the faster response time.")
    else:
        print(f"Across all messages, {B.name} has the faster response time.")

    print(f"Average response time of {A.name} is {A.avg_response_time}")
    print(f"Average response time of {B.name} is {B.avg_response_time}")

    labels = ["Monday", "Tuesday", 'Wednesday',
              "Thursday", "Friday", "Saturday", "Sunday"]
    A_values = [x.seconds for x in A.weekday_response_times.values()]
    B_values = [x.seconds for x in B.weekday_response_times.values()]

    x = np.arange(len(labels))  # the label locations
    width = 0.25  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width/2, A_values, width, label=A.name)
    rects2 = ax.bar(x + width/2, B_values, width, label=B.name)

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Average Response Time (in seconds)')
    ax.set_title(
        'Average Response Times across the week (in seconds)')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()

    autolabel(rects1, ax)
    autolabel(rects2, ax)

    fig.tight_layout()
    plt.show()
    print("Footnote about what a response is:\nA response is happening everytime the sender changes (so, if you write three messages, and then receive five in a row, that is still one response by the other person). \nAlso, if there is a break of at least 6 hours, it is assumed that this is not a response but rather a new conversation or the next morning.")


# def response_scatterplot(A, B):
    # values = []
    # for x in A.date_response_times.values():
    #    if type(x) == int:
    #        values.append(0)
    #    else:
    #        values.append(x.seconds)
    # Bvalues = []
    # for x in B.date_response_times.values():
    #    if type(x) == int:
    #        Bvalues.append(0)
    #    else:
    #        Bvalues.append(x.seconds)
    # plt.plot([str(x) for x in A.date_response_times.keys()], values, 'g^', [str(x) for x in A.date_response_times.keys()], Bvalues, 'bs')
    # plt.show()

def autolabel(rects, ax):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


def vocab_comparison(A, B):
    if len(A.vocab) > len(B.vocab):
        print(f"{A.name} has the larger vocabulary!")
    else:
        print(f"{B.name} has the larger vocabulary!")
    print(f"{A.name} used a vocabulary of {len(A.vocab)} unique words (counting different lemma).")
    print(f"{B.name} used a vocabulary of {len(B.vocab)} words.\n")
    A_vocab = pd.DataFrame(A.vocab[:A.print_top_x_used_words], columns=[
                           f"Top Words ({A.name})", "Count"], index=range(1, A.print_top_x_used_words+1))
    B_vocab = pd.DataFrame(B.vocab[:B.print_top_x_used_words], columns=[
                           f"Top Words ({B.name})", "Count"], index=range(1, B.print_top_x_used_words+1))
    display_side_by_side(A_vocab, B_vocab)


def display_side_by_side(*args):
    html_str = ''
    for df in args:
        html_str += df.to_html()
    display_html(html_str.replace(
        'table', 'table style="display:inline"'), raw=True)


def msg_timeline(A, B):
    a_msgs = list(A.date_msgs.values())
    b_msgs = list(B.date_msgs.values())
    ind = np.arange(len(A.date_msgs.keys()))  # the x locations for the groups
    # width = 0.35       # the width of the bars: can also be len(x) sequence

    p1 = plt.bar(ind, a_msgs)
    p2 = plt.bar(ind, b_msgs, bottom=a_msgs)

    plt.ylabel('Total Messages')
    plt.xlabel('Date')
    plt.xticks(ind, format_dates(A.date_msgs.keys()))
    plt.xticks(rotation=45)
    plt.title('Chronology of Messages')
    plt.legend((p1[0], p2[0]), (A.name, B.name))
    plt.show()


def format_dates(dates):
    if len(dates) >= 1000:
        date_seperator = 30
    elif len(dates) >= 500:
        date_seperator = 15
    elif len(dates) >= 400:
        date_seperator = 12
    elif len(dates) >= 300:
        date_seperator = 10
    elif len(dates) >= 100:
        date_seperator = 7
    elif len(dates) >= 50:
        date_seperator = 3
    else:
        date_seperator = 2

    formated = []
    for i, date in enumerate(dates):
        if i % date_seperator == 1:
            formated.append(str(date.day)+"."+str(date.month))
        else:
            formated.append(" ")
    return formated


def phrase_use(A, B):
    # Initialize the matplotlib figure
    _f, ax = plt.subplots(figsize=(6, 6))

    # Load the example car crash dataset
    phrase = pd.DataFrame([(phrase.name, phrase.count)
                           for phrase in A.phrases], columns=["Name", A.name])
    phraseB = pd.DataFrame([(phrase.name, phrase.count)
                            for phrase in B.phrases], columns=["Name", B.name])
    phrase[B.name] = phraseB[B.name]
    phrase["total"] = phrase[A.name]+phrase[B.name]
    phrase = phrase.sort_values("total", ascending=False)
    # Plot the total crashes
    sns.set_color_codes("pastel")
    sns.barplot("total", y="Name", data=phrase,
                label=A.name, color="b")

    # Plot the crashes where alcohol was involved
    sns.set_color_codes("muted")
    sns.barplot(x=B.name, y="Name", data=phrase,
                label=B.name, color="b")

    # Add a legend and informative axis label
    ax.legend(ncol=2, loc="lower right", frameon=True)
    ax.set(ylabel="", xlabel="Phrase Use")
    sns.despine(left=True, bottom=True)


def print_miscellaneous(A, B):
    vsA = str(A.avg_uninterrupted_texts)[
        :4] + " vs. " + str(B.avg_uninterrupted_texts)[:4]
    vsB = str(B.avg_uninterrupted_texts)[
        :4] + " vs. " + str(A.avg_uninterrupted_texts)[:4]
    if A.avg_uninterrupted_texts >= B.avg_uninterrupted_texts:
        print(f"{A.name} writes more texts in a row on average ({vsA})")
    else:
        print(f"{B.name} writes more texts in a row on average ({vsB})")
    print(
        f"The longest uninterrupted message chain (no media posts, foto albums, etc.) by {A.name} was {A.max_text_chain} messages long\n(written at {A.max_text_chain_timestamp}).")
    print(
        f"The lonsgest uninterrupted chain by {B.name} was {A.max_text_chain} messages long\n(written at {A.max_text_chain_timestamp}).")
