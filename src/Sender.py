import datetime
import re
import toml
import src.Phrase as Phrase
import src.misc as misc
from datetime import timedelta, date
import copy


class Sender:
    def __init__(self, name, chat):
        self.name = name
        self.message_count = 0
        self.media_count = 0
        self.word_count = 0
        self.vocab = {}
        self.print_top_x_used_words = 15
        self.message_sizes = []
        self.max_message_size = 0
        self.avg_message_size = 0
        self.max_message_size_time_timestamp = None
        self.response_times = []
        self.avg_response_time = 0
        self.text_chains = []
        self.max_text_chain = 0
        self.avg_uninterrupted_texts = 0
        self.max_text_chain_timestamp = None
        self.phrases = []
        self.load_phrases()
        self.first_day = chat[0].time
        self.last_day = chat[len(chat)-1].time
        self.weeks = misc.get_weeks_between_dates(
            self.first_day, self.last_day)
        self.days = misc.get_days_between_dates(self.first_day, self.last_day)
        self.weekday_words = {
            0: 0,
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
        }
        self.weekday_response_times = {
            0: [],
            1: [],
            2: [],
            3: [],
            4: [],
            5: [],
            6: [],
        }
        # gets a "day_ranges" chronological dict, where keys are the dates and values are int 0s, to count messages
        self.date_msgs = misc.get_day_range(
            self.first_day, self.last_day)  # stacked histogram
        # self.date_response_times = self.get_date_response_times()  # scatterplot
        #self.date_phrases = self.get_date_phrases()
        #  date_response_times is a chronological dict with lists instead of a int counters

    # def get_date_phrases(self):
    #    date_phrases = {}
    #    for phrase in self.phrases:
    #        date_phrases[phrase] = copy.deepcopy(self.date_msgs)
    #    return date_phrases

    def load_phrases(self):
        """looks into resources/phrases.toml and loads all active phrases which will get analyzed"""
        phrase_dict = toml.load("resources/phrases.toml")
        for phrase in phrase_dict:
            if phrase_dict[phrase]["active"]:
                self.phrases.append(Phrase.Phrase(phrase_dict[phrase]))

    # def get_date_response_times(self):
    #    date_range = copy.deepcopy(self.date_msgs)
    #    for date in date_range:
    #        date_range[date] = []
    #    return date_range

    def calc_averages(self):
        if self.message_sizes != []:
            self.avg_message_size = sum(self.message_sizes) / \
                len(self.message_sizes)
        if self.response_times != []:
            self.avg_response_time = sum(
                self.response_times, datetime.timedelta()) / len(self.response_times)
        if self.text_chains != []:
            self.avg_uninterrupted_texts = sum(
                self.text_chains) / len(self.text_chains)

        for weekday in self.weekday_words:
            if self.weekday_words[weekday] != 0:
                self.weekday_words[weekday] = self.weekday_words[weekday] / self.weeks
        for weekday in self.weekday_response_times:
            if self.weekday_response_times[weekday] == []:
                self.weekday_response_times[weekday] = 0
            else:
                self.weekday_response_times[weekday] = sum(
                    self.weekday_response_times[weekday], datetime.timedelta()) / len(self.weekday_response_times[weekday])
        # for day in self.date_response_times:
        #    if self.date_response_times[day] == []:
        #        self.date_response_times[day] = 0
        #    else:
        #        self.date_response_times[day] = sum(
        #            self.date_response_times[day], datetime.timedelta()) / len(self.date_response_times[day])

    def finalize(self):
        self.vocab["medium"] = 0
        self.vocab = misc.sort_dict_by_value(self.vocab)
        self.word_count = sum(self.message_sizes)

    def log(self, msg):
        self.log_msg_count(msg)
        self.log_phrases(msg)
        self.log_words(msg)

    def log_msg_count(self, msg):
        if msg.content == "media":
            self.media_count += 1
        else:
            self.message_count += 1
            self.date_msgs[msg.time.date()] += 1
            self.weekday_words[msg.time.weekday()] += len(msg.nlp_content)

    def log_chain_content(self, chat_log):
        self.text_chains.append(chat_log["chain"])
        if chat_log["chain"] > self.max_text_chain:
            self.max_text_chain = chat_log["chain"]
            self.max_text_chain_timestamp = chat_log["lastSendTime"]
        chat_log["chain"] = 1

    def log_words(self, msg):
        self.message_sizes.append(len(msg.nlp_content))
        if len(msg.nlp_content) > self.max_message_size:
            self.max_message_size = len(msg.nlp_content)
            self.max_message_size_time_timestamp = msg.time
        for token in msg.nlp_content:
            if not token.is_stop and not token.is_punct:
                self.vocab[token.lemma_] = self.vocab.get(token.lemma_, 0)+1

    def log_phrases(self, msg):
        for phrase in self.phrases:
            match = re.findall(phrase.regex, msg.content.lower())
            if match:
                phrase.count += 1
                #self.date_phrases[phrase][msg.time.date()] += 1
