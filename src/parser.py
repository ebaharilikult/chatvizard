from bs4 import BeautifulSoup
import os
import re
import src.Message as Message
from datetime import datetime
import pickle
import spacy
nlp = spacy.load("en_core_web_sm")


def read(path):
    with open(path, 'r', encoding="utf-8") as f:
        return f.read()


def get_folder_contents(directory):
    return os.listdir(directory)


def filter_non_html(folder_content):
    return list(filter(lambda file: file.endswith(".html"), folder_content))


def get_path(filenum, path):
    if filenum == 1:
        return path+"messages.html"
    else:
        return path+"messages" + str(filenum) + ".html"


def get_raw_data(path):
    raw_data = []
    for filenum in range(len(filter_non_html(get_folder_contents(path)))):
        filenum += 1
        raw_data.append(read(get_path(filenum, path)))
    return raw_data


def soupify_raw_data(raw_data):
    soupified_data = []
    for html in raw_data:
        soup = BeautifulSoup(html, 'html.parser')
        msgs = soup.findAll("div", {"class": "body"})
        soupified_data.extend(msgs)

    return soupified_data


def parse(msgs):
    print("parsing the chat data..")
    chat = []
    date_re = re.compile(
        r"[0-9]{2}\.[0-9]{2}\.\d{4} [0-9]{2}:[0-9]{2}:[0-9]{2}")
    forward_re = re.compile(r'<div class="forwarded body">')
    sender = None
    for msg in msgs:
        if not is_forwarded(forward_re, str(msg)):
            time = None
            content = None
            time_string = re.findall(date_re, str(msg))
            if time_string:
                time = datetime.strptime(time_string[0], '%d.%m.%Y %H:%M:%S')
            new_sender = msg.find("div", {"class": "from_name"})
            if new_sender:
                sender = cut_forwards(new_sender.get_text().strip()).strip()
            content = msg.find("div", {"class": "text"})
            if content:
                content = content.get_text().strip()
            if not content:
                content = "media"
            if time:
                chat.append(Message.Message(
                    time, sender, content, nlp(content)))
    return chat


def is_forwarded(forward_re, msg):
    match = re.findall(forward_re, msg)
    if match:
        return True
    else:
        return False


def cut_forwards(sender):
    compiled_pattern = re.compile(r" via .*")
    res = re.sub(compiled_pattern, "", sender)
    return res


def save(data, path):
    pickle.dump(data, open(
        path+"_data.dat", "wb"))
