import src.parser as parser

if __name__ == "__main__":
    path = "resources/ChatExport_03_03_2020/"
    path = "/home/dranokami/Documents/misc/anna/ChatExport_03_03_2020/"
    chat = parser.parse(parser.soupify_raw_data(parser.get_raw_data(path)))
    parser.save(chat, path)
    print(f"parsed and dumped {len(chat)} messages!")
